favorites = {
    "pet": "dog",
    "color": "red",
    "language": "Python",
}

"""res = favorites.setdefault("fruit", "apple")
print(res)
print(favorites)"""

res = favorites.setdefault("fruit", "apple")
print(res)
print(favorites)

res = favorites.get("season", "spring")
print(res)
print(favorites)

from collections import defaultdict

counter = defaultdict(int)

a = "dddfghjkl;lkjhgfdsdfghjklkjhgfdsdfgh"

for elem in a:
    counter[elem] += 1


print()

{1: 12, 2: 13, 3: 14} == {1: 12, 3: 14, 2: 13}

from collections import OrderedDict

dict_1 = OrderedDict()

dict_1["dog"] = "Sharik"
dict_1["cat"] = "Murka"
dict_1["cow"] = "Zirka"

for key, value in dict_1.items():
    print(f"{key} --> {value}")


dict_2 = OrderedDict()

dict_2["dog"] = "Sharik"
dict_2["cow"] = "Zirka"
dict_2["cat"] = "Murka"

print(dict_2 == dict_1)

dict_2.move_to_end("cow")

print(dict_2 == dict_1)

